package file

import (
	"testing"

	_ "gitlab.com/arewabolu/file.git"
)

func TestCreate(t *testing.T) {
	var f File
	fileres, _ := f.Create("names", "txt")

	if fileres != "names.txt" {
		t.Error("file created not given name, expected file is", fileres)
	}

}
