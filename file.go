package file

import (
	"log"
	"os"
)

//Create takes the name of the file
//and the file type to create a new file
func Create(name, ftype string) (string, *os.File) {
	fileName := name + "." + ftype
	newFile, err := os.Create(fileName)
	if err != nil {
		log.Fatal(err)
	}
	return fileName, newFile
}
